package org.hepeng.fabric.gateway.contract.spring;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.List;

/**
 * @author he peng
 * @date 2022/3/25
 */

@Data
@Configuration
@ConfigurationProperties(prefix = "hyperledger.fabric.contract")
public class ContractProperties {


    List<String> contractPackages;
}
