package org.hepeng.fabric.gateway.contract;

import org.hepeng.fabric.contract.ContractProvider;
import org.hepeng.fabric.gateway.pool.FabricGatewayHolder;
import org.hyperledger.fabric.client.Contract;

/**
 * @author he peng
 * @date 2022/3/24
 */
public class FabricGatewayHolderContractProvider implements ContractProvider {

    @Override
    public Contract getContract(String name) {
        return FabricGatewayHolder.current().getContract(name);
    }
}
