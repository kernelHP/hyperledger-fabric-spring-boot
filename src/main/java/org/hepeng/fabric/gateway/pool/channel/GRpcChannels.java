package org.hepeng.fabric.gateway.pool.channel;

import io.grpc.Channel;
import io.grpc.ManagedChannel;
import io.grpc.netty.shaded.io.grpc.netty.GrpcSslContexts;
import io.grpc.netty.shaded.io.grpc.netty.NettyChannelBuilder;
import org.hyperledger.fabric.client.identity.Identities;

import javax.net.ssl.SSLException;
import java.io.IOException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author he peng
 * @date 2022/3/24
 */
public abstract class GRpcChannels {

    private static final Map<String , ManagedChannel> CHANNEL_CACHE = new ConcurrentHashMap<>();

    public static synchronized Channel getChannel(GRpcChannelProperties properties) {

        ManagedChannel channel;
        if (CHANNEL_CACHE.containsKey(properties.getTarget())) {
            channel = CHANNEL_CACHE.get(properties.getTarget());
        } else {
            channel = newChannel(properties);
            CHANNEL_CACHE.put(properties.getTarget() , channel);

            Runtime.getRuntime().addShutdownHook(new Thread(channel::shutdownNow));
        }

        return channel;
    }

    private static ManagedChannel newChannel(GRpcChannelProperties properties) {

        try {
            X509Certificate tlsCert = Identities.readX509Certificate(properties.getX509TlsCertPem());
            return NettyChannelBuilder.forTarget(properties.getTarget())
                    .sslContext(GrpcSslContexts.forClient().trustManager(tlsCert).build())
                    .overrideAuthority(properties.getOverrideAuthority())
                    .build();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
