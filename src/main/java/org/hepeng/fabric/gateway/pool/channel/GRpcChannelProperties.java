package org.hepeng.fabric.gateway.pool.channel;

import lombok.Data;

/**
 * @author he peng
 * @date 2022/3/24
 */

@Data
public class GRpcChannelProperties {

    String x509TlsCertPem;

    String target;

    String overrideAuthority;
}
