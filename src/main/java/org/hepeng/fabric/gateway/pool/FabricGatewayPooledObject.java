package org.hepeng.fabric.gateway.pool;

import org.apache.commons.pool2.impl.DefaultPooledObject;

/**
 * @author he peng
 * @date 2022/3/24
 */
public class FabricGatewayPooledObject extends DefaultPooledObject<FabricGatewayObject> {


    /**
     * Creates a new instance that wraps the provided object so that the pool can
     * track the state of the pooled object.
     *
     * @param object The object to wrap
     */
    public FabricGatewayPooledObject(FabricGatewayObject object) {
        super(object);
    }
}
