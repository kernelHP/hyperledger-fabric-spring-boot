package org.hepeng.fabric.gateway.pool;

import lombok.Data;
import lombok.experimental.Accessors;
import org.hyperledger.fabric.client.Contract;
import org.hyperledger.fabric.client.Gateway;
import org.hyperledger.fabric.client.Network;

import java.util.Map;

/**
 * @author he peng
 * @date 2022/3/24
 */


@Data
@Accessors(chain = true)
public class FabricGatewayObject {

    GatewayKey key;

    Gateway gateway;

    Map<String , Network> networks;

    Map<String , Contract> contracts;


    public Contract getContract(String name) {
        return contracts.get(name);
    }

}
