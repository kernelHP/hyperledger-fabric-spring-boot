package org.hepeng.fabric.gateway.pool;

import lombok.Data;
import lombok.experimental.Accessors;


/**
 * @author he peng
 * @date 2022/3/24
 */

@Data
@Accessors(chain = true)
public class GatewayKey {

    String mspId;

    String channel;

    String chaincode;

    String id;

    String x509CertificatePem;

    String privateKeyPem;

}
