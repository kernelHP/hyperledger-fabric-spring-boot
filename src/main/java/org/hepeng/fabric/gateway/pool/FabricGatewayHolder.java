package org.hepeng.fabric.gateway.pool;


/**
 * @author he peng
 * @date 2022/3/24
 */
public class FabricGatewayHolder {

    private static final InheritableThreadLocal<FabricGatewayObject> FABRIC_GATEWAY_THREAD_LOCAL = new InheritableThreadLocal<>();

    public static void set(FabricGatewayObject fgo) {
        FABRIC_GATEWAY_THREAD_LOCAL.set(fgo);
    }

    public static FabricGatewayObject current() {
        return FABRIC_GATEWAY_THREAD_LOCAL.get();
    }

    public static FabricGatewayObject remove() {

        FabricGatewayObject current = current();
        FABRIC_GATEWAY_THREAD_LOCAL.remove();
        return current;
    }
}
