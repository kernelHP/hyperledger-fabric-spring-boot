package org.hepeng.fabric.gateway.pool;

/**
 * @author he peng
 * @date 2022/3/24
 */
public interface GatewayKeyProvider {

    GatewayKey getKey(Object key);
}
